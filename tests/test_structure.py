import unittest
import os
import glob
import shutil
import json
from config import PROJECT_DIR
from partialedge.algorithm import ped_algorithm as alg
from partialedge.graph import graph_creation as gcr


class BaseTest(unittest.TestCase):

    def setUp(self) -> None:
        self.path_resources = os.path.join(PROJECT_DIR, "tests/resources")
        self.path_graph = os.path.join(PROJECT_DIR, "tests/resources/graph")
        self.path_script = os.path.join(PROJECT_DIR, "tests/resources/script")
        self.path_output = os.path.join(PROJECT_DIR, "tests/out")

    def tearDown(self):
        test_junk = glob.glob(os.path.join(self.path_output, "*"))
        for content in test_junk:
            if os.path.isfile(content):
                os.remove(content)
            elif os.path.isdir(content):
                shutil.rmtree(content)


def load_graph():
    with open(os.path.join(PROJECT_DIR, "tests/resources/graph/graph.json"), "r") as f:
        graph = gcr.load_graph_from_graph_dict(json.load(f))
    return graph


def load_scenario(file_path):
    with open(os.path.join(PROJECT_DIR, "tests/resources", file_path), "r") as f:
        result_dict = json.load(f)

    return result_dict


def load_small_tree_decomposition_ped_scenario():
    result_dict = load_scenario("results/small_ped.json")
    suite = alg.TreeDecompositionAlgorithm(name="unittest", symmetric=False)
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_small_tree_decomposition_sped_scenario():
    result_dict = load_scenario("results/small_sped.json")
    suite = alg.TreeDecompositionAlgorithm(name="unittest", symmetric=True)
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_small_tree_sped_scenario():
    result_dict = load_scenario("results/small_tree_sped.json")
    suite = alg.TreeAlgorithm(name="unittest")
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_small_crossing_resolution_shped_scenario():
    result_dict = load_scenario("results/small_shped.json")
    suite = alg.CrossingResolutionAlgorithm(name="unittest")
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_large_tree_decomposition_ped_scenario():
    result_dict = load_scenario("results/large_ped.json")
    suite = alg.TreeDecompositionAlgorithm(name="unittest", symmetric=False)
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_large_tree_decomposition_sped_scenario():
    result_dict = load_scenario("results/large_sped.json")
    suite = alg.TreeDecompositionAlgorithm(name="unittest", symmetric=True)
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_large_tree_sped_scenario():
    result_dict = load_scenario("results/large_tree_sped.json")
    suite = alg.TreeAlgorithm(name="unittest")
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite


def load_large_crossing_resolution_shped_scenario():
    result_dict = load_scenario("results/large_shped.json")
    suite = alg.CrossingResolutionAlgorithm(name="unittest")
    suite.graph = gcr.load_graph_from_result_dict(result_dict)
    return result_dict, suite

