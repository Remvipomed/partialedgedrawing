import unittest
import os
import subprocess
import json
import glob
import platform
import sys
import matplotlib.pyplot as plt
from tests import test_structure as ts
from config import PROJECT_DIR
from partialedge.config import PACKAGE_DIR

PYTHON = "python3" if platform.system() == "Linux" else "python"

sys.path.append(PROJECT_DIR)
pypath = ''
for d in sys.path:
    pypath += d + ';'
ped_env = os.environ.copy()
ped_env['PYTHONPATH'] = pypath

path_main = os.path.join(PACKAGE_DIR, "ped.py")
path_script = os.path.join(PROJECT_DIR, "tests/resources/script")
path_single = os.path.join(path_script, "single/demo_single.json")
path_set = os.path.join(path_script, "set/demo_set.json")
path_dir = os.path.join(path_script, "dir")


class SingleTest(ts.BaseTest):

    def test_script_ped(self):
        subprocess.run([PYTHON, path_main, "--mode", "single", path_single], capture_output=True, check=True, env=ped_env)

    def test_script_sped(self):
        subprocess.run([PYTHON, path_main, "--mode", "single", "--symmetric", path_single], capture_output=True, check=True, env=ped_env)

    def test_script_shped(self):
        subprocess.run([PYTHON, path_main, "--mode", "single", "--symmetric", "--homogeneous", path_single], capture_output=True, check=True, env=ped_env)

    def test_script_output(self):
        subprocess.run([PYTHON, path_main, "--mode", "single", "--json", self.path_output, "--image", self.path_output, path_single], check=True, stderr=subprocess.PIPE, text=True, env=ped_env)

        with open(os.path.join(self.path_output, "demo_single.json"), "r") as f:
            result_dict = json.load(f)
        self.assertIsNotNone(result_dict)

        img = plt.imread(os.path.join(self.path_output, "demo_single.png"))
        self.assertIsNotNone(img)


class SetTest(ts.BaseTest):

    def test_script_ped(self):
        subprocess.run([PYTHON, path_main, "--mode", "set", path_set], capture_output=True,
                       check=True, env=ped_env)

    def test_script_sped(self):
        subprocess.run([PYTHON, path_main, "--mode", "set", "--symmetric", path_set],
                       capture_output=True, check=True, env=ped_env)

    def test_script_shped(self):
        subprocess.run(
            [PYTHON, path_main, "--mode", "set", "--symmetric", "--homogeneous", path_set],
            capture_output=True, check=True, env=ped_env)

    def test_script_output(self):
        subprocess.run([PYTHON, path_main, "--mode", "set", "--symmetric", "--json", self.path_output, "--image", self.path_output, path_set], check=True, stderr=subprocess.PIPE, text=True, env=ped_env)

        with open(os.path.join(self.path_output, "demo_set.json"), "r") as f:
            result_dict = json.load(f)
        self.assertIsNotNone(result_dict)

        imgs = glob.glob(self.path_output + "/*.png")
        self.assertEqual(10, len(imgs))


class DirectoryTest(ts.BaseTest):

    def test_script_ped(self):
        subprocess.run([PYTHON, path_main, "--mode", "dir", path_dir], capture_output=True, check=True, env=ped_env)

    def test_script_sped(self):
        subprocess.run([PYTHON, path_main, "--mode", "dir", "--symmetric", path_dir], capture_output=True, check=True, env=ped_env)

    def test_script_shped(self):
        subprocess.run([PYTHON, path_main, "--mode", "dir", "--symmetric", "--homogeneous", path_dir], capture_output=True, check=True, env=ped_env)

    def test_script_output(self):
        subprocess.run([PYTHON, path_main, "--mode", "dir", "--symmetric", "--homogeneous", "--json", self.path_output, "--image", self.path_output, path_dir], check=True, stderr=subprocess.PIPE, text=True, env=ped_env)

        jsons = glob.glob(os.path.join(self.path_output, "*.json"))
        self.assertEqual(5, len(jsons))

        images = glob.glob(os.path.join(self.path_output, "*/*.png"))
        self.assertEqual(50, len(images))


if __name__ == '__main__':
    unittest.main()
