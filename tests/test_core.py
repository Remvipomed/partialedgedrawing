import unittest
import os
import json
from tests import test_structure as ts
from partialedge.graph import graph_creation as gcr
from partialedge.core import tree, tree_decomp, crossing


class CreationTest(ts.BaseTest):

    def test_graph_creation_symmetric_with_treewidth(self):
        graph = gcr.create_random_with_tree_width(4, 15, 40, True)
        self.assertIsNotNone(graph)

    def test_graph_creation_asymmetric_with_treewidth(self):
        graph = gcr.create_random_with_tree_width(4, 10, 40, False)
        self.assertIsNotNone(graph)

    def test_graph_creation_with_tree_intersection(self):
        graph = gcr.create_random_with_tree_intersection(10, 20)
        self.assertIsNotNone(graph)

    def test_graph_creation(self):
        graph = gcr.create_random_graph(20, 40, "spring")
        self.assertIsNotNone(graph)

    def test_intersection_graph_creation(self):
        graph = ts.load_graph()
        intersection_graph = gcr.create_intersection_graph(graph.edges, False)
        self.assertEqual(20, len(intersection_graph.nodes))
        self.assertEqual(4, len(intersection_graph.edges))

    def test_content_node_creation(self):
        graph = ts.load_graph()
        intersection_graph = gcr.create_intersection_graph(graph.edges, False)
        content_nodes = gcr.create_content_nodes(intersection_graph)
        self.assertEqual(20, len(content_nodes))
        self.assertIsNotNone(content_nodes[0])

    def test_load_from_graph_file(self):
        with open(os.path.join(self.path_graph, "graph.json"), "r") as f:
            graph = gcr.load_graph_from_graph_dict(json.load(f))
        self.assertEqual(10, len(graph.nodes))
        self.assertEqual(20, len(graph.edges))

    def test_load_from_result_file(self):
        with open(os.path.join(self.path_graph, "result_graph.json"), "r") as f:
            graph = gcr.load_graph_from_result_dict(json.load(f))
        self.assertEqual(10, len(graph.nodes))
        self.assertEqual(20, len(graph.edges))


class AlgorithmTest(ts.BaseTest):

    def test_crossing_algorithm(self):
        graph = ts.load_graph()
        intersection_graph = gcr.create_intersection_graph(graph.edges, True)
        shped_value = crossing.calculate_symmetric_homogeneous(intersection_graph)
        self.assertAlmostEqual(0.3577033, shped_value, 4)

    def test_tree_algorithm(self):
        graph = ts.load_graph()
        intersection_graph = gcr.create_intersection_graph(graph.edges, True)
        roots = tree.form_forest(intersection_graph)
        result_dict = dict()

        roots[2].calculate_intersections()
        ink = roots[2].get_best_ink()
        roots[2].collect_result_ink(result_dict)

        self.assertAlmostEqual(2.899059, ink, 4)
        self.assertEqual(4, len(result_dict))

    def test_tree_decomposition_algorithm(self):
        graph = ts.load_graph()
        intersection_graph = gcr.create_intersection_graph(graph.edges, True)

        root_bag, tw = tree_decomp.form_tree_decomposition(intersection_graph, False)
        result_record = max(root_bag.calculate_records(), key=lambda x: x.get_ink_value())

        self.assertAlmostEqual(13.80225, result_record.get_ink_value(), 4)


if __name__ == '__main__':
    unittest.main()
