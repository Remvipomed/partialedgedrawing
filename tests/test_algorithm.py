import unittest
import json
import os

import matplotlib.pyplot as plt

from tests import test_structure as ts


class CreationTest(ts.BaseTest):

    def test_tree_decomposition_suite_ped_graph_creation(self):
        suite = ts.load_small_tree_decomposition_ped_scenario()[1]
        suite.create_graph(15, 25)
        self.assertEqual(len(suite.graph.nodes), 15)
        self.assertEqual(len(suite.graph.edges), 25)

    def test_tree_decomposition_suite_sped_graph_creation(self):
        suite = ts.load_small_tree_decomposition_sped_scenario()[1]
        suite.create_graph(15, 25)
        self.assertEqual(len(suite.graph.nodes), 15)
        self.assertEqual(len(suite.graph.edges), 25)

    def test_tree_suite_graph_creation(self):
        suite = ts.load_small_tree_sped_scenario()[1]
        suite.create_graph(15, 25)
        self.assertEqual(len(suite.graph.nodes), 15)
        self.assertEqual(len(suite.graph.edges), 25)

    def test_crossing_resolution_suite_shped_graph_creation(self):
        suite = ts.load_small_crossing_resolution_shped_scenario()[1]
        suite.create_graph(15, 25)
        self.assertEqual(len(suite.graph.nodes), 15)
        self.assertEqual(len(suite.graph.edges), 25)


class ExportTest(ts.BaseTest):

    def test_export_source_graph(self):
        result, suite = ts.load_small_tree_decomposition_ped_scenario()
        suite.export_source_graph(self.path_output)

        with open(os.path.join(self.path_resources, "sources/small_ped.json")) as f:
            expected = json.load(f)
        with open(os.path.join(self.path_output, "unittest.json"), "r") as f:
            actual = json.load(f)
        self.assertDictEqual(expected, actual)

    def test_export_json_result(self):
        result, suite = ts.load_small_tree_decomposition_ped_scenario()
        suite.perform_algorithm()
        suite.export_json_result(self.path_output)

        with open(os.path.join(self.path_output, "unittest.json"), "r") as f:
            actual = json.load(f)
        self.assertAlmostEqual(result["result"]["ink"], actual["result"]["ink"])

    def test_export_source_image(self):
        result, suite = ts.load_small_tree_decomposition_ped_scenario()
        suite.export_image_source(self.path_output)

        image = plt.imread(os.path.join(self.path_output, "unittest.png"))
        self.assertIsNotNone(image)

    def test_export_result_image(self):
        result, suite = ts.load_small_tree_decomposition_ped_scenario()
        suite.perform_algorithm()
        suite.export_image_result(self.path_output)

        image = plt.imread(os.path.join(self.path_output, "unittest.png"))
        self.assertIsNotNone(image)


class TreeDecompositionTest(ts.BaseTest):

    def test_small_ped(self):
        result, suite = ts.load_small_tree_decomposition_ped_scenario()
        suite.perform_algorithm()

        self.assertAlmostEqual(result["result"]["ink"], suite.result_ink)

    def test_small_sped(self):
        result, suite = ts.load_small_tree_decomposition_sped_scenario()
        suite.perform_algorithm()

        self.assertAlmostEqual(result["result"]["ink"], suite.result_ink)

    def test_large_ped(self):
        result, suite = ts.load_large_tree_decomposition_ped_scenario()
        suite.perform_algorithm()

        self.assertAlmostEqual(result["result"]["ink"], suite.result_ink)

    def test_large_sped(self):
        result, suite = ts.load_large_tree_decomposition_sped_scenario()
        suite.perform_algorithm()

        self.assertAlmostEqual(result["result"]["ink"], suite.result_ink)


class TreeAlgorithmTest(ts.BaseTest):

    def test_small_tree_sped(self):
        result, suite = ts.load_small_tree_sped_scenario()
        suite.perform_algorithm()

        self.assertDictEqual(result["result"], suite.result_edge_dict)

    def test_large_tree_sped(self):
        result, suite = ts.load_large_tree_sped_scenario()
        suite.perform_algorithm()

        self.assertDictEqual(result["result"], suite.result_edge_dict)


class CrossingResolutionTest(ts.BaseTest):

    def test_small_shped(self):
        result, suite = ts.load_small_crossing_resolution_shped_scenario()
        suite.perform_algorithm()

        self.assertDictEqual(result["result"], suite.result_edge_dict)

    def test_large_shped(self):
        result, suite = ts.load_large_crossing_resolution_shped_scenario()
        suite.perform_algorithm()

        self.assertDictEqual(result["result"], suite.result_edge_dict)


if __name__ == '__main__':
    unittest.main()
